
PShape pump1, pump2, pump3, pump4;

void setup() {
  size(500, 400);
  fill(0);
  pump1 = createShape();
  pump2 = createShape();
  pump3 = createShape();
  pump4 = createShape();
  
  pump1.beginShape();        //左目
  pump1.vertex(120, 100);
  pump1.vertex(60, 170);
  pump1.vertex(180, 180);
  pump1.endShape();
  
  pump2.beginShape();        //お鼻
  pump2.vertex(250, 170);
  pump2.vertex(200, 220);
  pump2.vertex(300, 220);
  pump2.endShape();
  
  pump3.beginShape();        //右目
  pump3.vertex(380, 100);
  pump3.vertex(320, 180);
  pump3.vertex(440, 170);
  pump3.endShape();
  
  pump4.beginShape();        //お口
  pump4.vertex(30, 250);
  pump4.vertex(110, 260);
  pump4.vertex(130, 280);
  pump4.vertex(150, 260);
  pump4.vertex(220, 270);
  pump4.vertex(250, 290);
  pump4.vertex(280, 270);
  pump4.vertex(350, 260);
  pump4.vertex(370, 280);
  pump4.vertex(390, 260);
  pump4.vertex(470, 250);
  pump4.vertex(420, 330);
  pump4.vertex(340, 370);
  pump4.vertex(320, 350);
  pump4.vertex(300, 370);
  pump4.vertex(200, 370);
  pump4.vertex(180, 350);
  pump4.vertex(160, 370);
  pump4.vertex(80, 330);
  pump4.vertex(30, 250);
}

void draw() {
  background(255, 187, 39);
  translate(mouseX -250, mouseY -200);
  shape(pump1, 0, 0);
  shape(pump2, 0, 0);
  shape(pump3, 0, 0);
  shape(pump4, 0, 0);
  textSize(50);                   //"HALLOWEEN"の文字
  fill(255);
  text("HALLOWEEN", 100, 335);
}
